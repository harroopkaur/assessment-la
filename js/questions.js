	const spanish = {
		'Control del  Inventario de Software': [
			{
				'Con que frecuencia realiza el levantamiento del inventario de aplicaciones instaladas en datacenter?': [
					{'Mensual': 2}, {'Trimestral': 2}, {'Annual': 1}, {'Nunca': 0}
				]
			},
			{
				'Con que frecuencia realiza el levantamiento del inventario de aplicaciones instaladas por los usuarios finales?': [
					{'Mensual': 2}, {'Trimestral': 2}, {'Annual': 1}, {'Nunca': 0}
				]
			},
			{
				'Tiene aceso para descubrir las aplicaciones instaladas por los usuarios finales?': [
					{'Si': 2}, {'No': 0}, {'Es imposible': 0}, {'No lo sé': 0}
				]
			},
			{
				'Tiene una alerta que le avise de cada nueva aplicacion instalada por los usuarios finales?': [
					{'Si': 2}, {'No': 0}, {'Es imposible': 0}, {'No lo sé': 0}
				]
			}
		],
		'Control de los Contratos de los Usuarios Finales': [
			{
				'Es posible acceder fácilmente a los datos de los contratos con los clientes finales?': [
					{'Si, contamos con un repositorio de datos': 2}, {'Si, puedo tener respuesta en 24 horas': 1},
					{'Es posible no tengamos todos los contratos': 1}, {'No tenemos un archivo de contratos': 0}
				]
			},
			{
				' Los contratos suscritos entre la empresa y sus clientes finales le permiten acceder a para conocer las instalaciones de software ?': [
					{'Si': 2}, {'No': 0}
				]
			},
			{
				'Cuenta su empresa con alguna alerta cuando se va a vencer el contrato de algún cliente final?': [
					{'Si': 2}, {'No': 0}, {'Lo hacemos manualmente': 1}
				]
			},
			{
				'Tienen sus contratos alguna cláusula de responsabilidad en donde el cliente final se responsabilice por instalaciones ilegales y las consecuencias de estas?': [
					{'Si': 2}, {'No': 0}, {'Hay una cláusula que pudiera servir pero es ambigua': 1}
				]
			},
		],
		'Control del Reporte Mensual': [
			{
				'Como elabora el Datacenter el reporte SPLA mensual?': [
					{'Manualmente': 0}, 
					{'Utilizan una herramienta de descubrimiento': 1}, 
					{'Automatizado': 2}, 
					{'Siempre se reporta lo mismo, solo pequeñas variaciones': 0}
				]
			},
			{
				'Existe alguna verificación entre lo reportado y lo desplegado en la infraestructura?': [
					{'Si': 2}, {'No': 0}, {'En ocasiones': 1}
				]
			},
			{
				'El personal encargado del reporte mensual SPLA, esta actualizado en el licenciamiento y las frecuentes actualizaciones?': [
					{'Si': 2}, {'No': 0}, {'No lo sé': 0}, {'En ocasiones presentan dudas': 1}
				]
			},
			{
				'Se revisan los reportes para determinar si hay errores de licenciamiento?': [
					{'Si': 2}, {'No': 0}, {'No lo sé': 0}
				]
			}
		],
		'Controles Generales': [
			{
				'Ha implementado los Key Performance Indicators (KPIs) de su software?': [
					{'Si': 2}, {'No': 0}, {'No lo sé': 0}
				]
			},
			{
				'Genera un reporte del inventario por dipositivo?': [
					{'Si': 2}, {'No': 0}, {'No lo sé': 0}
				]
			},
			{
				'La persona que administra el licenciamiento es certificada en todos los fabricantes?': [
					{'Si': 2}, {'No': 0}, {'No lo sé': 0}
				]
			},
			{
				'Tiene documentados los controles del proceso de control de software?': [
					{'Si': 2}, {'No': 0}, {'No lo sé': 0}
				]
			}
		],
		'Sobre la Infraestructura': [
			{
				'Cuál es el segmento del tamaño de la infraestuctura que aplica a su Datacenter:': [
					{'De 0 a 200 VM’s': 0}, {'De 200 a 500 VM’s': 1}, {'Más de 500 VM´s': 2}
				]
			},
			{
				'Cuál es la cantidad de clientes finales que aplica a su datacenter:': [
					{'De 0 a 200 Clientes finales': 0}, {'De 200 a 500 Clientes Finales': 1}, {'Más de 500 Clientes Finales': 2}
				]
			},
			{
				'Cuantos Datacenter administra su empresa:': [
					{'1 a 2': 0}, {'3 a 5': 1}, {'Mas de 5': 2}
				]
			},
			{
				'Que softwares maneja su datacenter': [
					{'Microsoft': 0}, {'VMWare': 1}, {'RedHat': 2}, {'Citrix': 3}, {'SAP': 4}
				]
			}
		],
		'Resultados': 0
	}