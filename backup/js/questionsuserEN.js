	const spanishuseren = {
		'Inventory': [
			{
				'Do you know the total number of active devices in your network (Servers, PCs, Laptops, Tablets)?': [
					{'Yes': 2}, {'No': 0}, {'Maybe': 1}
				]
			},
			{
				'Do you Know the software installed on all devices (Servers, PCs, Laptops, Tablets)?': [
					{'Yes': 2}, {'No': 0}, {'Maybe': 1}
				]
			},
			{
				'Do you know how much the value ($) of the software displayed represents?': [
					{'Yes': 2}, {'No': 0}, {'Maybe': 1}
				]
			}
		],
		'Purchases': [
			{
				'Do you know if your purchases are far above the equipments you own?': [
					{'Yes': 2}, {'No': 0}, {'Maybe': 1}
				]
			},
			{
				'Do you know if your purchases, adapt to new technologies and market requirements (in terms of versions, compatibility, security, reliability)?': [
					{'Yes': 2}, {'No': 0}, {'Only for some products': 1}
				]
			},
			{
				'Do you have centralized control of all your purchases / contracts?': [
					{'Yes': 2}, {'No': 0}, {'Only for some': 1}
				]
			},
			
		],
		'Licensing': [
			{
				'Are you able to identify if you currently have an over or sub-licensing of your software assets?': [
					{'Yes': 2}, {'No': 0}, {'Maybe': 1}
				]
			},
			{
				'Do you know how each of your software assets is licensed?': [
					{'Yes': 2}, {'No': 0}, {'Only some': 1}
				]
			},
			{
				'If an employee asks you for certain licensed software, can you know if you have a license available?': [
					{'Yes': 2}, {'No': 0}, {'Only of some': 1}
				]
			}
		],
		'Renewal': [
			{
				'Do you know the renewal date of each one of your software asset contracts?': [
					{'Yes': 2}, {'No': 0}, {'Only of some': 1}
				]
			},
			{
				'Do you have a site or repository where you can get information about your renovations quickly and effectively?': [
					{'Yes': 2}, {'No': 0}, {'Only for certain products': 1}
				]
			},
			{
				'Do you actually receive alerts or notifications of the next expiration of any of your software assets?': [
					{'Yes': 2}, {'No': 0}, {'Only for some': 1}
				]
			}
		],
		'Audit': [
			{
				'Do you know and understand the Audit clause of your software contracts?': [
					{'Yes': 2}, {'No': 0}
				]
			},
			{
				'Is you company prepared for a software audit?': [
					{'Yes': 2}, {'No': 0}
				]
			},
			{
				'How  many software audit have you faced in the last three years?': [
					{'From 0 to 3': 2}, {'From 4 to 6': 1}, {'More than 6': 0}
				]
			}
		],
		'Growth or Annuity': [
			{
				'Do you knowand understand the annuity clause (True Up) of your software contracts?': [
					{'Yes': 2}, {'No': 0}
				]
			},
			{
				'Do you know the Annuity Dates and / or true Up of your contracts?': [
					{'Yes': 2}, {'No': 0}, {'I don’t know': 0}
				]
			},
			{
				' Have you added new licenses in the last 12 months of your licensing agreement?': [
					{'Yes': 2}, {'No': 0}, {'I don’t know': 0}
				]
			}
		],
		'Result': 0
	}