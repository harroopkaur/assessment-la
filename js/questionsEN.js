	const spanishen = {
		'Software inventory control': [
			{
				'With what frequency is a data center installed applications  inventory carried out?': [
					{'Monthly': 2}, {'Quarterly': 2}, {'annualy': 1}, {'Never': 0}
				]
			},
			{
				'With what frequency is a end user installed applications inventory carried out?': [
					{'Monthly': 2}, {'Quarterly': 2}, {'annualy': 1}, {'Never': 0}
				]
			},
			{
				'Do you have access to a way to discover the applications installed by the end users of your equipment?': [
					{'Yes': 2}, {'No': 0}, {'Its impossible': 0}, {'I don’t know': 0}
				]
			},
			{
				'Do you have an alert to notify you of each new application installed by end users?': [
					{'Yes': 2}, {'No': 0}, {'Its impossible': 0}, {'I don’t know': 0}
				]
			}
		],
		'Final client contract control': [
			{
				'Is it possible to easily access  the data of the final client contracts?': [
					{'Yes, we have a data base': 2}, {'Yes, usually will have the information within 24 hours': 1},
					{'Its possible that we don’t have access to all the contracts': 1}, {'We don’t have a data base': 0}
				]
			},
			{
				'The contracts signed between the company and its final customers allow you to access to know the software facilities?': [
					{'Yes': 2}, {'No': 0}
				]
			},
			{
				'Does your bussiness count with an alert that notifies you  when the contract of a final client is going to expire?': [
					{'Yes': 2}, {'No': 0}, {'We do it manually': 1}
				]
			},
			{
				'Do you contracts have any liability clause where the end customer is responsible for illegal installations and the consequences of these?': [
					{'Yes': 2}, {'No': 0}, {'Theres a clause that could work but is ambiguous': 1}
				]
			},
		],
		'Monthly report control': [
			{
				'How does the Datacenter create the monthly SPLA report?': [
					{'Manually': 0}, 
					{'They use a discovery tool': 1}, 
					{'Automatically': 2}, 
					{'Its always the same report, with a few variations': 0}
				]
			},
			{
				"Is there any kind of verification between what's reported and  what's deployed in the infrastructure?": [
					{'Yes': 2}, {'No': 0}, {'Sometimes': 1}
				]
			},
			{
				'The personnel in charge of the monthly report SPLA, is updated in the licensing and frequent updates?': [
					{'Yes': 2}, {'No': 0}, {'I don’t know': 0}, {'Sometimes they have doubts': 1}
				]
			},
			{
				'The reports are reviewed to determine if there are licensing errors:': [
					{'Yes': 2}, {'No': 0}, {'I don’t know': 0}
				]
			}
		],
		'General Controls': [
			{
				'You have implemented the Key Performance Indicators (KPIs) of your software': [
					{'Yes': 2}, {'No': 0}, {'I don’t know': 0}
				]
			},
			{
				'You generate an inventory report by device?': [
					{'Yes': 2}, {'No': 0}, {'I don’t know': 0}
				]
			},
			{
				'The person who administers the licensing is certified in all manufacturers?': [
					{'Yes': 2}, {'No': 0}, {'I don’t know': 0}
				]
			},
			{
				'You have documented the controls of the software control process?': [
					{'Yes': 2}, {'No': 0}, {'I don’t know': 0}
				]
			}
		],
		'About the Infrastructure': [
			{
				'What is the size segment of the infrastructure that applies to your Datacenter?': [
					{"From 0 to 200 VM'S": 0}, {"From 200 to 500 VM'S": 1}, {"More than 500 VM'S": 2}
				]
			},
			{
				'What is the number of final customers that applies to your data center?': [
					{'From 0 to 200 final customers': 0}, {'From 200 to 500 final customers': 1}, {'More than 500 finals customers': 2}
				]
			},
			{
				'How many Datacenters does your company manage?': [
					{'From 1 to 2': 0}, {'From 3 to 5': 1}, {'More than 5': 2}
				]
			},
			{
				'What softwares does your datacenter manage?': [
					{'Microsoft': 0}, {'VMWare': 1}, {'RedHat': 2}, {'Citrix': 3}, {'SAP': 4}
				]
			}
		],
		'Result': 0
	}