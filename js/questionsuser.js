	const spanishuser = {
		'Inventario': [
			{
				'Sabe el total de equipos activos en su red (Servidores, PCs, Laptops, Tabletas)?': [
					{'Si': 2}, {'No': 0}, {'Tal vez': 1}
				]
			},
			{
				'Conoce los software instalados en todos los equipos (Servidores, PCs, Laptops, Tabletas)?': [
					{'Si': 2}, {'No': 0}, {'Tal vez': 1}
				]
			},
			{
				'Conoce cuánto representa el valor ($) del software desplegado?': [
					{'Si': 2}, {'No': 0}, {'Tal vez': 1}
				]
			}
		],
		'Compras': [
			{
				'Conoce si sus compras están muy por encima de las instalaciones?': [
					{'Si': 2}, {'No': 0}, {'Tal vez': 1}
				]
			},
			{
				'Conoce si sus compras, se adaptan a las nuevas tecnologías y a las exigencias del mercado (en cuanto a versiones, compatibilidad, seguridad, confiabilidad)?': [
					{'Si': 2}, {'No': 0}, {'Solo para determinados Productos': 1}
				]
			},
			{
				'Tiene usted un control centralizado de todas sus compras/contratos?': [
					{'Si': 2}, {'No': 0}, {'Solo para algunos': 1}
				]
			},
			
		],
		'Licenciamiento': [
			{
				'Es capaz de identificar si tiene actualmente un sobre o sub licenciamiento de sus activos de software?': [
					{'Si': 2}, {'No': 0}, {'Tal vez': 1}
				]
			},
			{
				'Conoce como se licencia cada uno de sus activos de software?': [
					{'Si': 2}, {'No': 0}, {'Conozco la forma de Licenciamiento de algunos': 1}
				]
			},
			{
				'Si un empleado le solicita un determinado software licenciable puede saber si se tiene licencia disponible?': [
					{'Si': 2}, {'No': 0}, {'Solo de algunos': 1}
				]
			}
		],
		'Renovación': [
			{
				'Conoce la fecha de renovación de cada uno sus contratos de activos de software?': [
					{'Si': 2}, {'No': 0}, {'De algunos': 1}
				]
			},
			{
				'Posee algún sitio o repositorio donde pueda obtener la información de sus renovaciones de manera rápida y efectiva': [
					{'Si': 2}, {'No': 0}, {'Solo para determinados productos': 1}
				]
			},
			{
				'Recibe alertas o notificaciones del próximo vencimiento de alguno de sus activos de software?': [
					{'Si': 2}, {'No': 0}, {'Solo de algunos recibo notificaciones': 1}
				]
			}
		],
		'Auditoría': [
			{
				'¿Conoce la cláusula de Auditoria de sus contratos de software?': [
					{'Si': 2}, {'No': 0}
				]
			},
			{
				'¿Esta su organización preparada para una auditoria de Software?': [
					{'Si': 2}, {'No': 0}
				]
			},
			{
				'¿Cuántas Auditorias de software ha afrontado en los últimos Tres años?': [
					{'0 -3 Auditorías': 2}, {'4 - 6 Auditorías': 1}, {'Mas de 6': 0}
				]
			}
		],
		'Crecimiento o Anualidad': [
			{
				'¿Conoce la cláusula de anualidad (True Up) de sus contratos de software?': [
					{'Si': 2}, {'No': 0}
				]
			},
			{
				'¿Conoce las Fechas de Anualidad y/o true Up de sus contratos?': [
					{'Si': 2}, {'No': 0}, {'Lo desconoce': 0}
				]
			},
			{
				'¿Ha incorporado nuevas licencias en los últimos 12 meses de su contrato de licenciamiento?': [
					{'Si': 2}, {'No': 0}, {'Lo desconoce': 0}
				]
			}
		],
		'Resultados': 0
	}