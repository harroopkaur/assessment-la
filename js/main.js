window.onload = function () {
	let user; let count = 4;
	let step = 0;
	let time;
	let current;
	let lastquiz;
	const question = document.getElementsByClassName('navi');
	const steps = document.getElementsByClassName('steps');
	const entity = document.getElementsByClassName('entity');
	const labels = ['Inventario', 'Usuario Final', 'Reportes', 'General'];
	const labelsen = ['Inventory', 'End user', 'Reports', 'General'];
	const labelsuser = ['Inventario', 'Licenciamiento', 'Auditoría', 'General'];
	const labelsuseren = ['Inventory', 'Licensing', 'Audit', 'General'];
	const status = ['Basico', 'Racionalizado', 'Estandarizado', 'Dinamico'];
	const statusen = ['Basic', 'Rationalized', 'Standard', 'Dynamic'];
	const bgcol = ['#0c0', '#0cf', '#4972bf', '#00cc00'];
	let result = {step0: [], step1: [], step2: [], step3: [], step4: [], step5: []};
	let butname = {step0: {}, step1: {}, step2: {}, step3: {}, step4: {}, step5: {}};
	const formc = {
		sp: {
			text: ['Uno de nuestros especialialistas puede darle mas detalle', 'Enviar', 'Obten tu informe', 'Aquí'],
			plsh: ['Nombre', 'Apellido', 'País', 'Empresa', 'Cargo', 'Email'],
			warn: 'Por favor complete todas las preguntas para avanzar'
		},
		en: {
			text: ['One of our specialists is ready to contact you and give you response right away', 'Send', 'Get your report', 'Here'],
			plsh: ['Name', 'Lastname', 'Country-City', 'Company Name', 'Job Tittle', 'Email'],
			warn: 'Please, complete your choise'
		}
	}
	const forma = document.getElementById('window2');
	let quiz = window.location.href.split('/')
			
	let language = 'sp';
	if(quiz[quiz.length - 1] === 'quiz.html') {
		current = spanish;
		document.getElementById('lan').onclick = function() {
			if(language === 'sp') {
				current = spanishen;
				language = 'en';
				document.getElementById('nex').innerText = "Next";
				document.getElementById('back').innerText = "Previous";
				document.getElementById('logotext').src = 'img/Datacenter Assestment.png';
				getreslan(language);
			}
			else {
				current = spanish;
				language = 'sp';
				document.getElementById('nex').innerText = "Siguiente";
				document.getElementById('back').innerText = "Atras";
				document.getElementById('logotext').src = 'img/Evaluacion Datacenter.png';
				getreslan(language);
			}
			for(var i = 0; i < count; i ++) {
				while(entity[i].children[2].firstChild) {
					entity[i].children[2].removeChild(entity[i].children[2].firstChild)
				}
			}
			render(current, step);
			getselect(); getsq(); gethiddiv(language);
		}
	}
	else{
		user = true; count = 3;
		current = spanishuser;
		document.getElementById('lan').onclick = function() {	
			if(language === 'sp') {
				current = spanishuseren;
				language = 'en';
				document.getElementById('nex').innerText = "Next";
				document.getElementById('back').innerText = "Previous";
				document.getElementById('logotext').src = 'img/End User Assestment.png';
				getreslan(language);
			}
			else {
				current = spanishuser;
				language = 'sp';
				document.getElementById('nex').innerText = "Siguiente";
				document.getElementById('back').innerText = "Atras";
				document.getElementById('logotext').src = 'img/Evaluacion Cliente Final.png';
				getreslan(language);
			}
			for(var i = 0; i < count; i ++) {
				while(entity[i].children[2].firstChild) {
					entity[i].children[2].removeChild(entity[i].children[2].firstChild)
				}
			}
			render(current, step);
			getselect(); getsq(); gethiddiv(language);
		}
	}
	function gethiddiv(lan) {
		let cont;
		if(lan === 'sp') {
			document.getElementById('op').innerHTML = '<b>Permitanos ayudarlo</b>';
			cont = formc.sp;
		}
		else {
			document.getElementById('op').innerHTML = '<b>Allow us to help you</b>';
			cont = formc.en;
		}
		let cont2;
		if(lan === 'sp') {
			document.getElementById('charname').innerHTML = '<b>Estimar los beneficios de la inversión</b>';
			cont2 = formc.sp;
		}
		else {
			document.getElementById('charname').innerHTML = '<b>Estimate benefits of investment</b>';
			cont2 = formc.en;
		}
		let cont3;
		if(lan === 'sp') {
			document.getElementById('Ventasdata').innerHTML = '<b>Ventas Perdidas</b>';
			cont3 = formc.sp;
		}
		else {
			document.getElementById('Ventasdata').innerHTML = '<b>Lost Sales</b>';
			cont3 = formc.en;
		}
		let cont4;
		if(lan === 'sp') {
			document.getElementById('Sobredata').innerHTML = '<b>Sobre Reporte</b>';
			cont4 = formc.sp;
		}
		else {
			document.getElementById('Sobredata').innerHTML = '<b>About Report</b>';
			cont4 = formc.en;
		}
		let cont5;
		if(lan === 'sp') {
			document.getElementById('Auditoriadata').innerHTML = '<b>Auditoria</b>';
			cont5 = formc.sp;
		}
		else {
			document.getElementById('Auditoriadata').innerHTML = '<b>Audit</b>';
			cont5 = formc.en;
		}
		let cont6;
		if(lan === 'sp') {
			document.getElementById('Totaldata').innerHTML = '<b>Total</b>';
			cont6 = formc.sp;
		}
		else {
			document.getElementById('Totaldata').innerHTML = '<b>Total</b>';
			cont6 = formc.en;
		}
		forma.children[2].innerHTML = '<b>'+cont.text[0]+'</b>';
		forma.children[7].children[0].children[0].innerHTML = '<b>'+cont.text[1]+'</b>';
		forma.children[8].children[0].children[0].innerHTML = '<b>'+cont.text[2]+'<span style="color: #595959">'+' '+cont.text[3]+'</span></b>';
		const inp = forma.getElementsByTagName('input')
		for(let i = 0; i < inp.length; i++) {
			inp[i].attributes[1].value = cont.plsh[i];
		}
		document.getElementsByClassName('warn')[0].children[0].innerHTML = cont.warn;
	}
	function getreslan(lan) {
		if(step === 5 && !user || step === 6 && user) {
			let group; let stat;
			let ll = document.getElementsByClassName('labels');
			if(!user && lan === 'sp') { group = labels; stat = status; }
			if(!user && lan === 'en') { group = labelsen; stat = statusen; }
			if(user && lan === 'sp') { group = labelsuser; stat = status; }
			if(user && lan === 'en') { group = labelsuseren; stat = statusen; }
			ll[0].innerText = group[0];
			ll[1].innerText = stat[0];
			ll[2].innerText = group[1];
			ll[3].innerText = stat[1];
			ll[4].innerText = group[2];
			ll[5].innerText = stat[2];
			ll[6].innerText = group[3];
			ll[7].innerText = stat[3];
			
		}
	}
	function getsq() {
		if(step === 5 && !user || step === 6 && user) {
			for(var i = 0; i < entity.length; i ++) {
				entity[i].children[2].innerHTML = '2';
			}
		}
	}
	let win;
	if(localStorage.lan === 'english'){

		if(quiz[quiz.length - 1] === 'quiz.html') {
			win = 'window1en';
			language = 'en';
			!user ? current = spanishen : current = spanishuseren;
			document.getElementById('nex').innerText = "Next";
			document.getElementById('back').innerText = "Previous";
			document.getElementById('logotext').src = 'img/Datacenter Assestment.png';
		}
		else{
			win = 'window1en';
			language = 'en';
			!user ? current = spanishen : current = spanishuseren;
			document.getElementById('nex').innerText = "Next";
			document.getElementById('back').innerText = "Previous";
			document.getElementById('logotext').src = 'img/End User Assestment.png';
		}


	}
	else {
		win = 'window1';
		language = 'sp';
		!user ? current = spanish : current = spanishuser;
	} 
	
	document.getElementById(win).style.visibility = 'visible';
	render(current, 0);
	
	document.getElementsByClassName('warn')[0].children[1].onclick = function() {
		document.getElementsByClassName('warn')[0].classList.remove('open');
	}
	const startq = document.getElementsByClassName('start');
	for(let i = 0; i < startq.length; i ++) {
		startq[i].onclick = function() {
			document.getElementById(win).style.visibility = 'hidden';
			document.getElementById('block').style.opacity = 0;
			document.getElementById('block').style.visibility = 'hidden';
			time = new Date();
		}
	}
	document.getElementsByClassName('start')[1].onclick = function() {
		document.getElementById(win).style.visibility = 'hidden';
		document.getElementById('block').style.opacity = 0;
		document.getElementById('block').style.visibility = 'hidden';
		time = new Date();
	}
	function render(obj, step) {
		(step === 0) ? document.getElementById('prev').style.visibility = 'hidden' : document.getElementById('prev').style.visibility = 'visible';
		Object.keys(obj).map(function(key, index) {
			question[index].innerText = key;
		});
		document.getElementById('title').innerText = Object.keys(obj)[step];
		steps[step].style.backgroundColor = '#fe9800';
		steps[step].style.borderColor = '#c55a11';
		document.getElementById('lab').children[step].style.color = '#002063';
		document.getElementById('lab').children[step].style.fontWeight = 'bold';
		document.getElementById('lab').children[step].classList.add('abc-sty');
		if(!user) {
			document.getElementById('picimg').src = 'img/' + (parseInt(step) + 1) + 't.png';
		}
		else {
			document.getElementById('picimg').src = 'img/' + (parseInt(step) + 1) + 'ut.png';
		}
		const current = obj[Object.keys(obj)[step]];
		let quest = [];
		for(let i = 0; i < current.length; i ++) {
			entity[i].children[0].innerText = Object.keys(current[i])[0];
			quest.push(current[i][Object.keys(current[i])[0]]);
		}
		for(let i = 0; i < quest.length; i++) {
			for(let y = 0; y < quest[i].length; y++) {
				let button = document.createElement('button');
				entity[i].children[2].appendChild(button);
				button.innerText = Object.keys(quest[i][y])[0];
				button.className = 'quizbut';
				let val = quest[i][y][Object.keys(quest[i][y])[0]];
				let ind = i;
				button.onclick = function() {
					choise(event, val, step, ind);
				}
			}
		}
	}
	function choise(e, value, step, ind) {
		document.getElementsByClassName('warn')[0].classList.remove('open');
		const but = e.target;
		const buttons = e.target.parentNode.getElementsByTagName('button');
		if(but.classList[1]) {
			but.classList.remove('checked');
			if(reviw(but.innerText)) {
				delete(result['step'+step][ind]);
			}
			return;
		}
		for(let i = 0; i < buttons.length; i ++) {
			if(!user && reviw(buttons[i].innerText) || user) {
				buttons[i].classList.remove('checked');
			}
		}
		function reviw(txt) {
			if(txt === 'Microsoft' || txt === 'VMWare' || txt === 'RedHat' || txt === 'Citrix' || txt === 'SAP') {
				return false;
			}
			else {
				return true;
			}
		}
		but.classList.add('checked');
		butname['step'+step][ind] = e.target.innerText;
		if(!user) { if(step < 5) { result['step'+step][ind] = value; } }
		else { if(step < 6) { result['step'+step][ind] = value; } }
	}
	document.getElementById('next').onclick = function() {
		document.getElementById('lab').children[step].style.color = '#a6a6a6';
		document.getElementById('lab').children[step].style.fontWeight = 'normal';
			document.getElementById('lab').children[step].classList.add('abc-sty');
		steps[step].style.backgroundColor = 'white';
		steps[step].style.borderColor = '#a6a6a6';
		const control = result['step'+step];
		if(!user) {
			if(control.length < 4 || typeof control[0] === 'undefined' || typeof control[1] === 'undefined' || typeof control[2] === 'undefined' || typeof control[3] == 'undefined') { 
				document.getElementsByClassName('warn')[0].classList.add('open');
				return; 
			}
		}
		else {
			if(control.length < 3 || typeof control[0] === 'undefined' || typeof control[1] === 'undefined' || typeof control[2] === 'undefined') { 
				document.getElementsByClassName('warn')[0].classList.add('open');
				return; 
			}
		}
		if(step === 4 && !user) {
			lastquiz = document.getElementById('content').innerHTML;
			res(current, 5);
			step ++;
			document.getElementById('lab').children[step].style.color = '#002063';
			document.getElementById('lab').children[step].style.fontWeight = 'bold';
				document.getElementById('lab').children[step].classList.add('abc-sty');
			return;
		}
		if(step === 5 && user) {
			lastquiz = document.getElementById('content').innerHTML;
			res(current, 6);
			step ++;
			document.getElementById('lab').children[step].style.color = '#002063';
			document.getElementById('lab').children[step].style.fontWeight = 'bold';
				document.getElementById('lab').children[step].classList.add('abc-sty');
			return;
		}
		for(var i = 0; i < count; i ++) {
			while(entity[i].children[2].firstChild) {
				entity[i].children[2].removeChild(entity[i].children[2].firstChild)
			}
		}
		render(current, step+1);
		step ++;
		document.getElementById('lab').children[step].style.color = '#002063';
		document.getElementById('lab').children[step].style.fontWeight = 'bold';
			document.getElementById('lab').children[step].classList.add('abc-sty');
		steps[step].style.backgroundColor = '#fe9800';
		steps[step].style.borderColor = '#c55a11';
		getselect();
	}
	document.getElementById('prev').onclick = function() {
		document.getElementById('backgr').style.visibility = 'visible';
		document.getElementById('content').removeAttribute('hidden');
		document.getElementById('getform').setAttribute('hidden', true);
		document.getElementById('window2').setAttribute('hidden', true);
		document.getElementById('contentchart').setAttribute('hidden', true);		
		document.getElementById('lab').children[step].style.color = '#a6a6a6';
		document.getElementById('lab').children[step].style.fontWeight = 'normal';
			document.getElementById('lab').children[step].classList.add('abc-sty');
		steps[step].style.backgroundColor = 'white';
		steps[step].style.borderColor = '#a6a6a6';
		if(step === 5 && !user) {
			getlastquiz();
		}
		if(step === 6 && user) {
			getlastquiz();
		}
		for(var i = 0; i < count; i ++) {
			while(entity[i].children[2].firstChild) {
				entity[i].children[2].removeChild(entity[i].children[2].firstChild)
			}
		}
		render(current, step-1);
		step --;
		document.getElementById('lab').children[step].style.color = '#002063';
		document.getElementById('lab').children[step].style.fontWeight = 'bold';
		steps[step].style.backgroundColor = '#fe9800';
		steps[step].style.borderColor = '#c55a11';
		getselect();
	};
	function getlastquiz() {
		document.getElementById('content').innerHTML = lastquiz;
		document.getElementById('next').style.visibility = 'visible';
		document.getElementById('backgr').style.visibility = 'visible';
		document.getElementById('getform').setAttribute('hidden', true);
	}
	function getselect() {
		for(let i = 0; i < entity.length; i ++) {
			let butt = entity[i].getElementsByTagName('button');
			for(let y = 0; y < butt.length; y ++) {
				if(butt[y].innerText === butname['step' + step][i]) {
					butt[y].classList.add('checked');
				}
			}
		}
	}
	function res(obj, step) {
		steps[step - 1].style.backgroundColor = 'white';
		steps[step - 1].style.borderColor = '#a6a6a6';
		document.getElementById('next').style.visibility = 'hidden';
		document.getElementById('prev').style.visibility = 'visible';
		document.getElementById('title').innerText = Object.keys(obj)[step];
		steps[step].style.backgroundColor = '#fe9800';
		steps[step].style.borderColor = '#c55a11';
		document.getElementById('picimg').src = 'img/6t.png';
		let st;
		user ? st = 3 : st = 4;
		for(let i = 0; i < st; i ++) {
			while(entity[i].firstChild) {
				entity[i].removeChild(entity[i].firstChild);
			}
		}
		for(let y = 0; y < 4; y ++) {
			column(y)
		}
		time = new Date() - time;
		result.time = parseInt(time / 1000) + ' s';
		getchart();
	}
	function column(index) {
		let total = 0;
		if(!user) {
			total = result['step'+index];
		}
		else {
			if(index === 0) { total = result['step0']; }
			if(index === 1) { total = result['step2']; }
			if(index === 2) { total = result['step4']; }
			if(index === 3) { total = result['step5']; }
		}
		
		if(user) {
			total = total[0] + total[1] + total[2];
			document.getElementById('hident').removeAttribute('hidden');
			var ent = document.getElementsByClassName('entity') 
			for(let y = 0; y < 4; y ++) {
				ent[y].classList.add('resulto');
			}
		}
		else {
			total = total[0] + total[1] + total[2] + total[3];
		}
		let val = 0;
		!user ? val = Math.ceil(total / 2 - 1) : val = Math.ceil(total / 2)
		var div = [];
		for(var i = 0; i < 10; i ++) {
			div[i] = document.createElement('div');
			entity[index].appendChild(div[i]);
		}
		if(val === 0 || val < 0) {
			val = 0;
		}
		const img = document.createElement('img');
		if(!user) {
			img.src = 'img/resu'+(parseInt(index)+1)+'.png';
		}
		else {
			if(index === 0) { img.src = 'img/storage.png'; }
			if(index === 1) { img.src = 'img/certificate.png'; }
			if(index === 2) { img.src = 'img/audit.png'; }
			if(index === 3) { img.src = 'img/stadistics.png'; }
		}
		img.style.width = "100%";
		div[0].appendChild(img);
		div[0].style.borderRadius = '100px';
		div[0].style.border = '0.2vw solid #a6a6a6';
		div[0].style.width = '30%';
		div[0].style.marginLeft = '35%';
		div[1].className = 'labels';
		if(!user && language === 'sp') { div[1].innerText = labels[index]; }
		if(!user && language === 'en') { div[1].innerText = labelsen[index]; }
		if(user && language === 'sp') { div[1].innerText = labelsuser[index]; }
		if(user && language === 'en') { div[1].innerText = labelsuseren[index]; }
		let x;
		user ? x = 0 : x = 2;
		for(let i = 2; i < 6; i ++) {
			div[i].innerText = x;
			x = x + 2;
			div[i].className = 'sqar';
			div[i].height = div[i].offsetWidth + 'px';
		}
		div[7].style.height = '2vh';
		div[7].style.width = '99%';
		div[7].style.border = '1px solid grey';
		div[7].style.marginTop = '1vh';
		div[7].style.float = 'left'
		div[7].appendChild(div[8]);
		div[8].style.height = '100%';
		div[8].style.backgroundColor = bgcol[val];
		user && val !== 0 ? div[8].style.width = total * 10 + 40 + '%' : div[8].style.width = total * 10 + 20 + '%';
		div[9].className = 'labels';
		language === 'sp' ? div[9].innerText = status[val] : div[9].innerText = statusen[val];
		let sq = val + 2;
		div[sq].style.backgroundColor = bgcol[val];
	}
	document.getElementById('openform').onclick = function() {
		document.getElementById('backgr').style.visibility = 'visible';
		document.getElementById('content').setAttribute('hidden', true);
		document.getElementById('getform').setAttribute('hidden', true);
		document.getElementById('window2').removeAttribute('hidden');
	}
	document.getElementById('closeform').onclick = function() {
		document.getElementById('backgr').style.visibility = 'hidden';
		document.getElementById('content').removeAttribute('hidden');
		document.getElementById('getform').removeAttribute('hidden');
		document.getElementById('window2').setAttribute('hidden', true);
	}
	document.getElementById('send').onclick = function() {
		butname.user = {};
		const inp = forma.getElementsByTagName('input')
		for(let i = 0; i < inp.length; i++) {
			butname.user[inp[i].attributes[1].value] = inp[i].value;
		}
		butname.time = result.time;
		xml_http = new XMLHttpRequest();
		xml_http.open('POST', '../ajax.php')
		xml_http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xml_http.send('form='+ JSON.stringify(butname));
		xml_http.onreadystatechange = function() {
			if (xml_http.readyState == 4) {
				if(xml_http.response) {
					alert('Successfully send');
				}
				else {
					alert('Unable to send message, please try again');
				}
				document.getElementById('closeform').onclick();
			}
		}
	}














	
	function getchart() {
		document.getElementById('backgr').style.visibility = 'hidden';
		document.getElementById('getform').removeAttribute('hidden');
		if(user) { return; }
		document.getElementById('contentchart').removeAttribute('hidden');
		const invest = result.step4[0];
		let values = { step0: 0, step1: 0, step2: 0, step3: 0 };
		Object.keys(result).map(function(key, index) {
			if(key =='step0' || key =='step1' || key =='step2' || key =='step3') {
				for(let i = 0; i < 4; i ++) {
					values[key] = parseInt(values[key]) + parseInt(result[key][i]);
				}
				values[key] =  parseInt(parseInt(values[key]) / 2) - 1;
				parseInt(values[key]) < 1 ? values[key] = 0 : values[key];
			}
		});
		let chartotal = {};
		let round = 0;
		for(let i = 0; i < 4; i ++) {
			chartotal[i] = chres[i][invest][values['step'+i]];
			round = round + parseInt(chartotal[i]);
		}
		round = round/100;
		let per = 0;
		let graph = {0: 0, 1: 0, 2: 0, 3: 0};
		for(let i = 0; i < 4; i ++) {
			graph[i] = parseInt(parseInt(chartotal[i]) / round);
			per = per + parseInt(graph[i]);
		}
		graph[1] = parseInt(graph[1]) + 100 - per;






		for(let i = 0; i < 4; i ++) {
			document.getElementsByClassName('tit')[i].children[0].innerHTML = '<b> - '+chartotal[i]+'$K</b>';
			//document.getElementsByClassName('tit')[i].children[0].innerHTML = '('+graph[i]+'%)<b> - '+chartotal[i]+'$K</b>';
		}

		graph[3] = 0;
		getsectors(graph);



var pie_val_1 = chartotal[0];
var pie_val_2 = chartotal[1];
var pie_val_3 = chartotal[2];


var data = [
  ['good', pie_val_1],
  ['bad', pie_val_2],
  ['neutral', pie_val_3]
];

var chart = c3.generate({
    bindto: '#piechart',
    data: {
        columns: data,
        type: 'pie'
    },
    legend: {
        show: false
    },
    // tooltip: {
    //   position: function (data, width, height, element) {
    //     return {top: 0, right: 0}
    //   }
    // },
    onrendered: function(){
      var self = this;
      console.log(self)
      
      d3Pie = d3.select('.c3-chart-arcs');
      pieSize = d3Pie.node().getBBox();
      pieTransform = d3.transform(d3Pie.attr("transform"));

      // MODIFY PIE POSITION
      var posX = 0+pieSize.width/1.5;
      var posY = pieTransform.translate[1];
      d3Pie.attr('transform', 'translate('+posX+','+posY+')')
    }
});

// ADD CUSTOM LEGEND
// var d3legend = d3.select('.c3-chart').append('g')
//   .attr('transform', 'translate('+(pieSize.width + pieSize.width/3)+',100)')
//   .attr('class', 'custom-legend')
//   .selectAll('g')
//   .data(data)
//   .enter()
//   .append('g')
//   .attr('transform', function(d, i){
//     return 'translate(0, '+i*60+')';
//   })
//   .attr('data-id', function (d) { 
//     return d[0]; 
//   })
//   .on('mouseover', function (id) {
//     chart.focus(id);
//   })
//   .on('mouseout', function (id) {
//     chart.revert();
//   })

// var legendRect = d3legend.append('rect')
//   .attr('class', function(d, i){
//     return 'custom-legend-color is-'+d[0];
//   })
//   .attr('width', 40)
//   .attr('height', 40)
//   .attr('rx', 4)
//   .attr('ry', 4)
//   .style('fill', function(d, i){
//     return chart.color(d[0])
//   });

// var legendValue = d3legend.append('text')
//   .attr('class', 'custom-legend-value')
//   .attr('x', 50)
//   .text(function(d, i){
//     return (d[1]/100)*100 + '$K';
//   });

// var legendTitle = d3legend.append('text')
//   .attr('class', 'custom-legend-title')
//   .attr('x', 50)
//   .attr('y', 30)
//   .attr('font-size', '15px')
//   .text(function(d, i){
//     return d[0];
//   });    





	}






















































	function getsectors(values) {
		var myCanvas = document.getElementById("chart");
		myCanvas.width = 300;
		myCanvas.height = 300;
		myCanvas.style.transform = 'rotate(-90deg)';
		var ctx = myCanvas.getContext("2d");
		function drawLine(ctx, startX, startY, endX, endY){
			ctx.beginPath();
			ctx.moveTo(startX,startY);
			ctx.lineTo(endX,endY);
			ctx.stroke();
		}
		function drawArc(ctx, centerX, centerY, radius, startAngle, endAngle){
			ctx.beginPath();
			ctx.arc(centerX, centerY, radius, startAngle, endAngle);
			ctx.stroke();
		}
		function drawPieSlice(ctx,centerX, centerY, radius, startAngle, endAngle, color ){
			ctx.fillStyle = color;
			ctx.beginPath();
			ctx.moveTo(centerX,centerY);
			ctx.arc(centerX, centerY, radius, startAngle, endAngle);
			ctx.closePath();
			ctx.fill();
		}
		var Piechart = function(options){
			this.options = options;
			this.canvas = options.canvas;
			this.ctx = this.canvas.getContext("2d");
			this.colors = options.colors;
		 	this.draw = function(){
				var total_value = 0;
				var color_index = 0;
				for (var categ in this.options.data){
					var val = this.options.data[categ];
					total_value += val;
				}
				var start_angle = 0;
				for (categ in this.options.data){
					val = this.options.data[categ];
					var slice_angle = 2 * Math.PI * val / total_value;
					drawPieSlice(
						this.ctx,
						this.canvas.width/2,
						this.canvas.height/2,
						Math.min(this.canvas.width/2,this.canvas.height/2),
						start_angle,
						start_angle+slice_angle,
						this.colors[color_index%this.colors.length]
					);
					start_angle += slice_angle;
					color_index++;
				}
		 
			}
		}
		var myPiechart = new Piechart({
			canvas: myCanvas,
			data: values,
			colors: bgcol
		});
		myPiechart.draw();
	}
}












































































































